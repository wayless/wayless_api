#![no_std]

#[macro_use]
extern crate mashup;

#[allow(unused)]
pub mod std;

pub mod invocation;

pub mod syscall;

//Create an extra module for bindings to reduce name conflicts
pub mod sel4 {
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(unused)]
    include!("bindings.rs");
}



