//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


// This ugly macro lets us turn all of the invocation labels from seL4
// into members of an enum. seL4 divides these labels up into three
// categories, but the distinction isn't useful so I stick them into
// one big enum
macro_rules! labels {
    ( $($prefix:ident { $($label:ident),+ $(,)*}),+ $(,)* ) => {
        //We use this mashup macro so we can concatenate stuff
        //concat_idents can't be used in pattern position
        mashup! {
            $(
                $(
                    rename[$prefix "label" $label] = $prefix _label_ $label;
                 )+
            )+
        }

        #[derive(Debug,PartialEq)]
        pub enum InvocationLabel {
            //Assign seL4's syscall number to each variant of Syscall
            //$($syscall = concat_idents!(syscall_, $syscall) as isize),+,
            $(
                $($label = rename!(crate::sel4::$prefix"label"$label) as isize),+,
            )+
        }
        impl InvocationLabel {
            //This function lets us convert from label number
            //to a InvocationLabel
            pub fn from_u32(label_num: u32) -> Option<InvocationLabel> {
                use self::InvocationLabel::*;

                match label_num {
                    //Turn a syscall number from seL4 into a Syscall
                    $($(
                        rename!(crate::sel4::$prefix"label"$label) => 
                        Some($label)),+,)+
                    _ => None
                }
            }
        }
    }
}

//All of the labels from seL4, combined into one big enum
//TODO: finish filling this out
labels!{
    invocation {
        InvalidInvocation,
    },
    arch_invocation {
        X86IOPortOut8,
        X86IOPortOut16,
        X86IOPortOut32,
        X86IOPortIn8,
        X86IOPortIn16,
        X86IOPortIn32,
    }
}

